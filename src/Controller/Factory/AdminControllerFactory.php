<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 07/11/2017
 * Time: 13:22
 */
namespace User\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use User\Controller\AdminController;
use User\Service\UserManager;
use Application\Service\ActivityManager;
use Application\Service\ImageManager;

/**
 * This is the factory for User AdminController. Its purpose is to instantiate the controller
 * and handle dependency injection.
 */
class AdminControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $activityManager = $container->get(ActivityManager::class);
        $imageManager = $container->get(ImageManager::class);

        return new AdminController($entityManager, $userManager, $activityManager, $imageManager);
    }
}