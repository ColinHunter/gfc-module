<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter4
 * Date: 02/06/2018
 * Time: 13:03
 */
namespace User\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use User\Controller\PermissionController;
use User\Service\PermissionManager;

/**
 * This is the factory for PermissionController. Its purpose is to instantiate the controller
 * and handle dependency injection.
 */
class PermissionControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $permissionManager = $container->get(PermissionManager::class);

        return new PermissionController($entityManager, $permissionManager);
    }
}