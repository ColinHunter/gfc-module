<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 07/11/2017
 * Time: 13:00
 */
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use User\Entity\User;
use User\Entity\Role;
use Blog\Entity\Post;
use Application\Entity\Quotes;
use Application\Entity\Country;
use Project\Entity\Project;
use Article\Entity\Article;
use User\Form\UserForm;
use User\Form\PasswordChangeForm;
use User\Form\PasswordResetForm;

/**
 * This controller is responsible for user management
 * (add, edit,viewing users and changing users passwords).
 */
class UserController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager.
     * @var User\Service\UserManager
     */
    private $userManager;

    /**
     * Constructor.
     */
    public function __construct($entityManager, $userManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
    }

    /**
     * This is the default "index" action of the controller.
     * It displays a list of users.
     */
    public function indexAction()
    {
        // Access control.
        if (!$this->access('user.manage')) {
            $this->getResponse()->setStatusCode(401);
            return;
        }

        $users = $this->entityManager->getRepository(User::class)
            ->findBy([], ['id'=>'ASC']);

        return new ViewModel([
            'users' => $users
        ]);
    }

    /**
     * Action to add a new user.
     */
    public function addAction()
    {
        // Create user form
        $form = new UserForm('create', $this->entityManager);

        // Get the list of all available roles (sorted by name).
        $allRoles = $this->entityManager->getRepository(Role::class)
            ->findBy([], ['name'=>'ASC']);
        $roleList = [];
        foreach ($allRoles as $role) {
            $roleList[$role->getId()] = $role->getName();
        }

        if($form->get('roles')->getAttribute('type') != 'hidden') {
            $form->get('roles')->setValueOptions($roleList);
        }

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid()) {

                // Get filtered and validated data
                $data = $form->getData();

                // Add user.
                $user = $this->userManager->addUser($data);

                // Redirect to "message" page
                if($user) {
                    return $this->redirect()->toRoute('users', ['action'=>'message', 'id'=>'userAdd']);
                } else {
                    return $this->redirect()->toRoute('users', ['action'=>'message', 'id'=>'userAddFail']);
                }

            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * This action validates new user email and updates the User status.
     */
    public function newUserAction()
    {
        $token = $this->params()->fromQuery('token', null);
        $id = $this->params()->fromQuery('id', null);

        // Validate token length
        if ($token!=null && (!is_string($token) )) {
            throw new \Exception('Invalid token');
        }

        if($token===null ||
            !$this->userManager->validatePasswordResetToken($id, $token, true)) {
            return $this->redirect()->toRoute('users',
                ['action'=>'message', 'id'=>'failedvalid']);
        }

        // Set new password for the user.
        if ($this->userManager->setNewUserByToken($id, $token)) {

            // Redirect to "message" page
            return $this->redirect()->toRoute('users',
                ['action'=>'message', 'id'=>'setuser']);
        } else {
            // Redirect to "message" page
            return $this->redirect()->toRoute('users',
                ['action'=>'message', 'id'=>'faileduser']);
        }
    }

    /**
     * Action to view a user's details.
     */
    public function viewAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $quotes = $this->entityManager->getRepository(Quotes::class)
            ->findApprovedQuotes();

        // Find user by ID.
        $user = $this->entityManager->getRepository(User::class)
            ->find($id);

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $countries = $this->entityManager->getRepository(Country::class)->findAll();
        $flag = '';
        $userCountry = $user->getCountry();
        foreach($countries as $country) {
            if($user->getCountry() === $country->getCountryName()) {
                $flag = $country->getCountryCode() == 'AB' ? 'scotland' : strtolower($country->getCountryCode());
            }
        }

        $projects = $this->entityManager->getRepository(Project::class)
            ->findPublishedProjectsByUser($user->getId());

        $articles = $this->entityManager->getRepository(Article::class)
            ->findPublishedArticlesByUser($user->getId());

        $posts = $this->entityManager->getRepository(Post::class)
            ->findPublishedPostsByUser($user->getId());
        $displayPosts = array();
        $year = '';
        $month = '';
        foreach($posts as $post) {
            $year = date('Y', strtotime($post->getDateCreated()));
            $month = date('F', strtotime($post->getDateCreated()));
            if(!array_key_exists($year, $displayPosts)) {
                $displayPosts[$year] = array();
            }
            if(!array_key_exists($month, $displayPosts[$year])) {
                $displayPosts[$year][$month] = array();
            }
            if(!array_key_exists($post->getId(), $displayPosts[$year][$month])) {
                $displayPosts[$year][$month][$post->getId()] = $post;
            }
        }

        return new ViewModel([
            'user' => $user,
            'quotes' => $quotes,
            'flag' => $flag,
            'projects' => $projects,
            'articles' => $articles,
            'posts' => $displayPosts
        ]);
    }

    /**
     * Action to edit user.
     */
    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $user = $this->entityManager->getRepository(User::class)
            ->find($id);

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Create user form
        $form = new UserForm('update', $this->entityManager, $user, $this->identity());

        // Get the list of all available roles (sorted by name).
        $allRoles = $this->entityManager->getRepository(Role::class)
            ->findBy([], ['name'=>'ASC']);
        $roleList = [];
        foreach ($allRoles as $role) {
            $roleList[$role->getId()] = $role->getName();
        }

        if (!$this->access('user.manage')) {
            return $this->redirect()->toRoute('not-authorized');
        } else {
            // ToDo Check errors on this for auth / editor roles
            $form->get('roles')->setValueOptions($roleList);
        }

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid()) {

                // Get filtered and validated data
                $data = $form->getData();

                // Update the user.
                $caller = 'edit';
                $this->userManager->updateUser($user, $data, $caller);

                // Redirect to "view" page
                return $this->redirect()->toRoute('users',
                    ['action'=>'view', 'id'=>$user->getId()]);
            }
        } else {

            $userRoleIds = [];
            foreach ($user->getRoles() as $role) {
                $userRoleIds[] = $role->getId();
            }

            $form->setData(array(
                'first_name'=>$user->getFirstName(),
                'last_name'=>$user->getLastName(),
                'email'=>$user->getEmail(),
                'status'=>$user->getStatus(),
                'roles' => $userRoleIds
            ));
        }

        return new ViewModel(array(
            'user' => $user,
            'form' => $form
        ));
    }

    /**
     * Action to change a user's password from the admin zone.
     */
    public function changePasswordAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $user = $this->entityManager->getRepository(User::class)
            ->find($id);

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Create "change password" form
        $form = new PasswordChangeForm('change');

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid()) {

                // Get filtered and validated data
                $data = $form->getData();

                // Try to change password.
                if (!$this->userManager->changePassword($user, $data)) {
                    $this->flashMessenger()->addErrorMessage(
                        'Sorry, the old password is incorrect. Could not set the new password.');
                } else {
                    $this->flashMessenger()->addSuccessMessage(
                        'Password changed successfully.');
                }

                // Redirect to "view" page
                return $this->redirect()->toRoute('users',
                    ['action'=>'view', 'id'=>$user->getId()]);
            }
        }

        return new ViewModel([
            'user' => $user,
            'form' => $form
        ]);
    }

    /**
     * Action to reset password via the forgot password link.
     */
    public function resetPasswordAction()
    {
        // Create form
        $form = new PasswordResetForm();

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid()) {

                // Get the user by email.
                $user = $this->entityManager->getRepository(User::class)
                    ->findOneByEmail($data['email']);
                if ($user!=null) {
                    // Generate a new password for user
                    // and send an E-mail notification.
                    $this->userManager->generatePasswordResetToken($user);

                    // Redirect to "message" page
                    return $this->redirect()->toRoute('users',
                        ['action'=>'message', 'id'=>'sent']);
                } else {
                    return $this->redirect()->toRoute('users',
                        ['action'=>'message', 'id'=>'invalid-email']);
                }
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * This action displays the "Reset Password" page.
     */
    public function setPasswordAction()
    {
        $token = $this->params()->fromQuery('token', null);
        $id = $this->params()->fromQuery('id', null);

        // Validate token length
        if ($token!=null && (!is_string($token) )) {
            throw new \Exception('Invalid token');
        }

        if($token===null ||
            !$this->userManager->validatePasswordResetToken($id, $token)) {
            return $this->redirect()->toRoute('users',
                ['action'=>'message', 'id'=>'failed']);
        }

        // Create form
        $form = new PasswordChangeForm('reset');

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid()) {

                $data = $form->getData();

                // Set new password for the user.
                if ($this->userManager->setNewPasswordByToken($id, $token, $data['new_password'])) {

                    // Redirect to "message" page
                    return $this->redirect()->toRoute('users',
                        ['action'=>'message', 'id'=>'set']);
                } else {
                    // Redirect to "message" page
                    return $this->redirect()->toRoute('users',
                        ['action'=>'message', 'id'=>'failed']);
                }
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * This action displays an information message page.
     */
    public function messageAction()
    {
        // Get message ID from route.
        $id = (string)$this->params()->fromRoute('id');

        // Validate input argument.
        if($id!='invalid-email' && $id!='sent' && $id!='set' && $id!='failed' && $id!='userAdd' && $id!='userAddFail' && $id!='setuser' && $id!='faileduser' && $id!='failedvalid') {
            throw new \Exception('Invalid message ID specified');
        }

        return new ViewModel([
            'id' => $id
        ]);
    }

    public function onDispatch(MvcEvent $e)
    {
        // Call the base class' onDispatch() first and get the response
        $response = parent::onDispatch($e);

        $call = $this->getRequest()->getUriString();

        // Set alternative layouts
        if(strpos($call, 'view') > 1  ) {
            $this->layout()->setTemplate ('/layout/layout');
        } else {
            $this->layout()->setTemplate ('/layout/admin');
        }

        // Return the response
        return $response;
    }
}