<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 19/06/2018
 * Time: 14:59
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use User\Entity\User;
use Application\Entity\Country;
use User\Form\UserForm;


class AdminController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager.
     * @var user\Service\UserManager
     */
    private $userManager;

    /**
     * Activity manager.
     * @var Application\Service\ActivityManager
     */
    private $activityManager;

    /**
     * Image manager.
     * @var Application\Service\ImageManager
     */
    private $imageManager;

    // The constructor is used for dependency injection.
    public function __construct($entityManager, $userManager, $activityManager, $imageManager)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->activityManager = $activityManager;
        $this->imageManager = $imageManager;
    }

    public function profileUpdateAction()
    {
        $id = (int)$this->params()->fromRoute('id', -1);
        if ($id<1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $user = $this->entityManager->getRepository(User::class)
            ->find($id);

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $countries = $this->entityManager->getRepository(Country::class)->findAll();
        $countryArray = array();
        foreach ($countries as $country) {
            $countryArray[$country->getId()] = $country->getCountryName();
        }

        $user->setBiog(html_entity_decode($user->getBiog()));

        // Create user form
        $form = new UserForm('updateProfile', $this->entityManager, $user, $this->identity(), $countryArray);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if($form->isValid()) {

                // Get filtered and validated data
                $data = $form->getData();

                // Update the user.
                $caller = 'profile';
                $data['biog'] = htmlentities ($data['biog']);
                $data['country'] = $countryArray[$data['country']];
                $updated = $this->userManager->updateUser($user, $data, $caller);

                // Redirect to "view" page
                return $this->redirect()->toRoute('users',
                    ['action'=>'view', 'id'=>$user->getId()]);
            }
        } else {

            $countryId = 0;
            foreach($countryArray as $key=>$country) {
                if($user->getCountry() === $country){
                    $countryId = $key;
                }
            }

            $form->setData(array(
                'biog'=>$user->getBiog(),
                'first_name'=>$user->getFirstName(),
                'last_name'=>$user->getLastName(),
                'address_1'=>$user->getAddress1(),
                'address_2'=>$user->getAddress2(),
                'city'=>$user->getCity(),
                'county'=>$user->getCounty(),
                'country'=>$countryId,
                'post_code'=>$user->getPostCode(),
                'show_address'=>$user->getShowAddress(),
                'show_email'=>$user->getShowEmail(),
                'status'=>$user->getStatus(),
                'phone'=>$user->getPhone(),
                'mobile'=>$user->getMobile(),
                'skype'=>$user->getSkype(),
                'show_contact'=>$user->getShowContact(),
                'website'=>$user->getWebsite(),
                'youtube'=>$user->getYoutube(),
                'image_location' => $user->getProfilePic()
            ));
        }

        return new ViewModel(array(
            'user' => $user,
            'form' => $form,
        ));
    }

    public function onDispatch(MvcEvent $e)
    {
        // Call the base class' onDispatch() first and get the response
        $response = parent::onDispatch($e);

        // Set alternative layout
        $this->layout()->setTemplate ('/layout/adminUpload');

        // Return the response
        return $response;
    }
}