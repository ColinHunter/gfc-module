<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 02/06/2018
 * Time: 13:09
 */
namespace User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use User\Entity\User;

/**
 * This controller plugin is designed to get the currently logged in User entity
 * from inside a controller.
 */
class CurrentUserPlugin extends AbstractPlugin
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Authentication service.
     * @var Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * Logged in user.
     * @var User\Entity\User
     */
    private $user = null;

    /**
     * Constructor.
     */
    public function __construct($entityManager, $authService)
    {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
    }

    /**
     * This method is called when you invoke this plugin in your controller: $user = $this->currentUser();
     * @param bool $useCachedUser If true, the User entity is fetched only on the first call (and cached on subsequent calls).
     * @return User|null
     */
    public function __invoke($useCachedUser = true)
    {
        // If current user is already fetched, return it.
        if ($useCachedUser && $this->user!==null)
            return $this->user;

        // Check if user is logged in.
        if ($this->authService->hasIdentity()) {

            // Fetch User entity from database.
            $this->user = $this->entityManager->getRepository(User::class)
                ->findOneByEmail($this->authService->getIdentity());
            if ($this->user==null) {
                // If the identity is present in the session, but there is no such user in the database.
                // Throw an exception. This is possibly a security issue.
                throw new \Exception('Not found user with such email');
            }

            // Return the found User.
            return $this->user;
        }

        return null;
    }
}