<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 07/11/2017
 * Time: 13:01
 */
namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This class represents a registered user.
 * @ORM\Entity()
 * @ORM\Table(name="user")
 */
class User
{
    // User status constants.
    const STATUS_ACTIVE       = 1; // Active user.
    const STATUS_RETIRED      = 2; // Retired user.
    const STATUS_NEW          = 3; // New user, to be activated.

    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="email")
     */
    protected $email;

    /**
     * @ORM\Column(name="first_name")
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name")
     */
    protected $lastName;

    /**
     * @ORM\Column(name="image_location")
     */
    protected $profilePic;

    /**
     * @ORM\Column(name="bio_body")
     */
    protected $biog;


    /**
     * @ORM\Column(name="address_1")
     */
    protected $address1;

    /**
     * @ORM\Column(name="address_2")
     */
    protected $address2;

    /**
     * @ORM\Column(name="city")
     */
    protected $city;

    /**
     * @ORM\Column(name="county_state")
     */
    protected $county;

    /**
     * @ORM\Column(name="country")
     */
    protected $country;

    /**
     * @ORM\Column(name="post_code")
     */
    protected $postCode;

    /**
     * @ORM\Column(name="show_address")
     */
    protected $showAddress;

    /**
     * @ORM\Column(name="show_email")
     */
    protected $showEmail;

    /**
     * @ORM\Column(name="phone")
     */
    protected $phone;

    /**
     * @ORM\Column(name="mobile")
     */
    protected $mobile;

    /**
     * @ORM\Column(name="skype")
     */
    protected $skype;

    /**
     * @ORM\Column(name="show_contact")
     */
    protected $showContact;

    /**
     * @ORM\Column(name="website")
     */
    protected $website;

    /**
     * @ORM\Column(name="youtube")
     */
    protected $youtube;

    /**
     * @ORM\Column(name="password")
     */
    protected $password;

    /**
     * @ORM\Column(name="status")
     */
    protected $status;

    /**
     * @ORM\Column(name="date_created")
     */
    protected $dateCreated;

    /**
     * @ORM\Column(name="pwd_reset_token")
     */
    public $passwordResetToken;

    /**
     * @ORM\Column(name="pwd_reset_token_creation_date")
     */
    protected $passwordResetTokenCreationDate;

    /**
     * @ORM\ManyToMany(targetEntity="User\Entity\Role")
     * @ORM\JoinTable(name="user_role",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *      )
     */
    public $roles;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * Returns user ID.
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets user ID.
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns email.
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets email.
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns first name.
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets first name.
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Returns last name.
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets last name.
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Returns image_location.
     * @return string
     */
    public function getProfilePic()
    {
        return $this->profilePic;
    }

    /**
     * Sets image_location.
     * @param string $profilePic
     */
    public function setProfilePic($profilePic)
    {
        $this->profilePic = $profilePic;
    }

    /**
     * Returns biog.
     * @return string
     */
    public function getBiog()
    {
        return $this->biog;
    }

    /**
     * Sets biog.
     * @param string $biog
     */
    public function setBiog($biog)
    {
        $this->biog = $biog;
    }

    /**
     * Returns address 1.
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Sets $address 1.
     * @param string $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * Returns address 2.
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Sets $address 2.
     * @param string $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * Returns city.
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets city.
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns county / state.
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Sets county / state.
     * @param string $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * Returns country.
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets country.
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns post Code.
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Sets post Code.
     * @param string $postCode
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    }

    /**
     * Returns show Address.
     * @return bool
     */
    public function getShowAddress()
    {
        return $this->showAddress;
    }

    /**
     * Sets show Address.
     * @param bool $showAddress
     */
    public function setShowAddress($showAddress)
    {
        $this->showAddress = $showAddress;
    }

    /**
     * Returns show Email.
     * @return bool
     */
    public function getShowEmail()
    {
        return $this->showEmail;
    }

    /**
     * Sets show Email.
     * @param bool $showEmail
     */
    public function setShowEmail($showEmail)
    {
        $this->showEmail = $showEmail;
    }

    /**
     * Returns phone.
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets phonee.
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns mobile.
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets mobile.
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Returns skype.
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Sets skype.
     * @param string $skype
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
    }

    /**
     * Returns Show Contact.
     * @return string
     */
    public function getShowContact()
    {
        return $this->showContact;
    }

    /**
     * Sets Show Contact.
     * @param string $showContact
     */
    public function setShowContact($showContact)
    {
        $this->showContact = $showContact;
    }

    /**
     * Returns website.
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets website.
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * Returns youtube.
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Sets youtube.
     * @param string $youtube
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
    }

    /**
     * Returns status.
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns possible statuses as array.
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_RETIRED => 'Retired',
            self::STATUS_NEW => 'New'
        ];
    }

    /**
     * Returns user status as string.
     * @return string
     */
    public function getStatusAsString()
    {
        $list = self::getStatusList();
        if (isset($list[$this->status]))
            return $list[$this->status];

        return 'Unknown';
    }

    /**
     * Sets status.
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Returns password.
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets password.
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the date of user creation.
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Sets the date when this user was created.
     * @param string $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * Returns password reset token.
     * @return string
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * Sets password reset token.
     * @param string $token
     */
    public function setPasswordResetToken($token)
    {
        $this->passwordResetToken = $token;
    }

    /**
     * Returns password reset token's creation date.
     * @return string
     */
    public function getPasswordResetTokenCreationDate()
    {
        return $this->passwordResetTokenCreationDate;
    }

    /**
     * Sets password reset token's creation date.
     * @param string $date
     */
    public function setPasswordResetTokenCreationDate($date)
    {
        $this->passwordResetTokenCreationDate = $date;
    }

    /**
     * Returns the array of roles assigned to this user.
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Returns the string of assigned role names.
     */
    public function getRolesAsString()
    {
        $roleList = '';

        $count = count($this->roles);
        $i = 0;
        foreach ($this->roles as $role) {
            $roleList .= $role->getName();
            if ($i<$count-1)
                $roleList .= ', ';
            $i++;
        }

        return $roleList;
    }

    /**
     * Assigns a role to user.
     */
    public function addRole($role)
    {
        $this->roles->add($role);
    }

    public function getFullName()
    {
        $firstName = $this->getFirstName();
        $lastName = $this->getLastName();
        return $firstName . ' ' . $lastName;
    }
}