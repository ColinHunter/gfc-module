<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 02/06/2018
 * Time: 13:47
 */
namespace User\View\Helper;

use Zend\View\Helper\AbstractHelper;
use User\Entity\User;

/**
 * This view helper is used for retrieving the Users entity of currently logged in user.
 */
class CurrentUser extends AbstractHelper
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Authentication service.
     * @var Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * Previously fetched User entity.
     * @var User\Entity\User
     */
    public $user = null;

    /**
     * Constructor.
     */
    public function __construct($entityManager, $authService)
    {
        $this->entityManager = $entityManager;
        $this->authService = $authService;
    }

    /**
     * Returns the current User or null if not logged in.
     * @param bool $useCachedUser If true, the User entity is fetched only on the first call (and cached on subsequent calls).
     * @return User|null
     */
    public function __invoke($useCachedUser = true)
    {
        // Check if User is already fetched previously.
        if ($useCachedUser && $this->user!==null)
            return $this->user;

        // Check if user is logged in.
        if ($this->authService->hasIdentity()) {

            // Fetch User entity from database.
            $this->user = $this->entityManager->getRepository(User::class)->findby(array(
                'email' => $this->authService->getIdentity()
            ));
            if ($this->user==null) {
                // The identity is present in the session, but not found in the database.
                // Throw an exception, because this is a possibly a security issue.
                throw new \Exception('User Not found');
            }

            // Return the User entity we found.
            return $this->user;
        }

        return null;
    }
}