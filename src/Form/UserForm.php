<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 07/11/2017
 * Time: 13:03
 */
namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\ArrayInput;
use User\Validator\UserExistsValidator;
use Zend\Captcha;

/**
 * This form is used to collect user's email, first name, last name, which combined = full name, password and status.
 * The form can work in two scenarios - 'create' and 'update'. In the 'create' scenario, password is required,
 * in the 'update' scenario password is not required.
 * in the 'create' scenario status is hidden & set to 'active', in the update scenario 'status' is available to edit.
 */
class UserForm extends Form
{
    /**
     * Scenario ('create' or 'update').
     * @var string
     */
    private $scenario;

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * Current edit user.
     * @var User\Entity\User
     */
    private $user = null;

    /**
     * Current logged user email.
     * @var string
     */
    private $identity = null;

    /**
     * Countries array.
     * @var array
     */
    private $countryArray = null;

    /**
     * Constructor.
     */
    public function __construct($scenario = 'create', $entityManager = null, $user = null, $identity = null, $countryArray = null)
    {
        // Define form name
        parent::__construct('user-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->scenario = $scenario;
        $this->entityManager = $entityManager;
        $this->user = $user;
        $this->identity = $identity;
        $this->countryArray = $countryArray;

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "email" field
        $this->add([
            'type'  => 'text',
            'name' => 'email',
            'options' => [
                'label' => 'E-mail',
            ],
        ]);

        // Add "first_name" field
        $this->add([
            'type'  => 'text',
            'name' => 'first_name',
            'options' => [
                'label' => 'First Name',
            ],
        ]);

        // Add "last_name" field
        $this->add([
            'type'  => 'text',
            'name' => 'last_name',
            'options' => [
                'label' => 'Last Name',
            ],
        ]);

        if ($this->scenario == 'create') {

            // Add "password" field
            $this->add([
                'type'  => 'password',
                'name' => 'password',
                'options' => [
                    'label' => 'Password',
                ],
            ]);

            // Add "confirm_password" field
            $this->add([
                'type'  => 'password',
                'name' => 'confirm_password',
                'options' => [
                    'label' => 'Confirm password',
                ],
            ]);

            // Add "status" field
            $this->add([
                'type'  => 'hidden',
                'name' => 'status',
                'options' => [
                    'label' => 'Status',
                ],
                'attributes' => [
                    'label' => 'status',
                    'value' => 3
                ],
            ]);

            /* Add the CAPTCHA field
            $this->add([
                'type'  => 'captcha',
                'name' => 'captcha',
                'attributes' => [
                ],
                'options' => [
                    'label' => 'Human check',
                    'captcha' => [
                        'class' => 'Image',
                        'imgDir' => 'public/img/captcha',
                        'suffix' => '.png',
                        'imgUrl' => '/img/captcha/',
                        'imgAlt' => 'CAPTCHA Image',
                        'font'   => './data/font/thorne_shaded.ttf',
                        'fsize'  => 24,
                        'width'  => 350,
                        'height' => 100,
                        'expiration' => 600,
                        'dotNoiseLevel' => 40,
                        'lineNoiseLevel' => 3
                    ],
                ],
            ]);
            */

            $this->add([
                'type' => 'Zend\Form\Element\Captcha',
                'name' => 'captcha',
                'options' => [
                    'label' => 'Please verify you are human',
                    'captcha' => new Captcha\Dumb(),
                ],
            ]);

            // Add the CSRF field
            $this->add([
                'type'  => 'csrf',
                'name' => 'csrf',
                'options' => [
                    'csrf_options' => [
                        'timeout' => 600
                    ]
                ],
            ]);

        } elseif ($this->scenario == 'update' ) {
            // Add "roles" field
            $this->add([
                'type'  => 'select',
                'name' => 'roles',
                'attributes' => [
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'label' => 'Role(s)',
                ],
            ]);

            // Add "status" field
            $this->add([
                'type'  => 'select',
                'name' => 'status',
                'options' => [
                    'label' => 'Status',
                    'value_options' => [
                        1 => 'Active',
                        2 => 'Retired',
                    ]
                ],
            ]);

        } elseif($this->scenario == 'updateProfile') {
            // Add "status" field
            $this->add([
                'type'  => 'select',
                'name' => 'status',
                'options' => [
                    'label' => 'Status',
                    'value_options' => [
                        1 => 'Active',
                        2 => 'Retired',
                    ]
                ],
            ]);

            // Add Biog Body
            $this->add([
                'type'  => 'textarea',
                'name' => 'biog',
                'attributes' => [
                    'id' => 'biog'
                ],
                'options' => [
                    'label' => 'Personal Statement',
                ],
            ]);

            // Add "address_1" field
            $this->add([
                'type'  => 'text',
                'name' => 'address_1',
                'options' => [
                    'label' => 'address 1',
                ],
            ]);

            // Add "address_2" field
            $this->add([
                'type'  => 'text',
                'name' => 'address_2',
                'options' => [
                    'label' => 'address 2',
                ],
            ]);

            // Add "city" field
            $this->add([
                'type'  => 'text',
                'name' => 'city',
                'options' => [
                    'label' => 'City',
                ],
            ]);

            // Add "county" field
            $this->add([
                'type'  => 'text',
                'name' => 'county',
                'options' => [
                    'label' => 'County / State',
                ],
            ]);

            // Add "country" field
            $this->add([
                'type'  => 'select',
                'name' => 'country',
                'options' => [
                    'label' => 'Country',
                    'value_options' => $this->countryArray
                ],
            ]);

            // Add "post_code" field
            $this->add([
                'type'  => 'text',
                'name' => 'post_code',
                'options' => [
                    'label' => 'Post Code / Zip',
                ],
            ]);

            // Add "show_address" field
            $this->add([
                'type'  => 'checkbox',
                'name' => 'show_address',
                'options' => [
                    'label' => 'Show Address on your profile page',
                ],
            ]);

            // Add "show_email" field
            $this->add([
                'type'  => 'checkbox',
                'name' => 'show_email',
                'options' => [
                    'label' => 'Show Email on your profile page',
                ],
            ]);

            // Add "phone" field
            $this->add([
                'type'  => 'text',
                'name' => 'phone',
                'options' => [
                    'label' => 'Phone',
                ],
            ]);

            // Add "mobile" field
            $this->add([
                'type'  => 'text',
                'name' => 'mobile',
                'options' => [
                    'label' => 'Mobile',
                ],
            ]);

            // Add "skype" field
            $this->add([
                'type'  => 'text',
                'name' => 'skype',
                'options' => [
                    'label' => 'Skype',
                ],
            ]);

            // Add "show_contact" field
            $this->add([
                'type'  => 'checkbox',
                'name' => 'show_contact',
                'options' => [
                    'label' => 'Show Contact Details on your profile page',
                ],
            ]);

            // Add "website" field
            $this->add([
                'type'  => 'text',
                'name' => 'website',
                'options' => [
                    'label' => 'Web site',
                ],
            ]);

            // Add "youtube" field
            $this->add([
                'type'  => 'text',
                'name' => 'youtube',
                'options' => [
                    'label' => 'YouTube',
                ],
            ]);

            $this->add([
                'type'  => 'hidden',
                'name' => 'image_location',
                'options' => [
                    'label' => 'image_location',
                ],
                'attributes' => [
                    'label' => 'image_location',
                    'id' => 'image_location',
                    'value' => $this->user->getProfilePic()
                ],
            ]);

        }

        if($this->user && $this->identity === 'admin@example.com') {
            // Add "roles" field
            $this->add([
                'type'  => 'select',
                'name' => 'roles',
                'attributes' => [
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'label' => 'Role(s)',
                ],
            ]);
        } elseif($this->user) {
            // Add "roles" field
            $userRoleIds = [];
            foreach ($this->user->getRoles() as $role) {
                $userRoleIds[] = $role->getId();
            }
            $this->add([
                'type'  => 'hidden',
                'name' => 'role',
                'options' => [
                    'label' => 'Role',
                ],
                'attributes' => [
                    'label' => 'roles',
                    'value' => $userRoleIds
                ],
            ]);
        } else {
            // Add "roles" field
            $this->add([
                'type'  => 'hidden',
                'name' => 'roles',
                'options' => [
                    'label' => 'Role(s)',
                ],
                'attributes' => [
                    'label' => 'roles',
                    'value' => 2
                ],
            ]);
        }

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create'
            ],
        ]);
    }

    /**
     * This method creates input filters (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        //ToDo Add validation for profile update fields

        // Add input for "first_name" field
        $inputFilter->add([
            'name'     => 'first_name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 512
                    ],
                ],
            ],
        ]);

        // Add input for "last_name" field
        $inputFilter->add([
            'name'     => 'last_name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 512
                    ],
                ],
            ],
        ]);

        if ($this->scenario == 'create') {

            // Add input for "email" field
            $inputFilter->add([
                'name'     => 'email',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 128
                        ],
                    ],
                    [
                        'name' => 'EmailAddress',
                        'options' => [
                            'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                            'useMxCheck'    => false,
                        ],
                    ],
                    [
                        'name' => UserExistsValidator::class,
                        'options' => [
                            'entityManager' => $this->entityManager,
                            'user' => $this->user
                        ],
                    ],
                ],
            ]);

            // Add input for "password" field
            $inputFilter->add([
                'name'     => 'password',
                'required' => true,
                'filters'  => [
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'min' => 6,
                            'max' => 64
                        ],
                    ],
                ],
            ]);

            // Add input for "confirm_password" field
            $inputFilter->add([
                'name'     => 'confirm_password',
                'required' => true,
                'filters'  => [
                ],
                'validators' => [
                    [
                        'name'    => 'Identical',
                        'options' => [
                            'token' => 'password',
                        ],
                    ],
                ],
            ]);
        }

        // Add input for "status" field
        $inputFilter->add([
            'name'     => 'status',
            'required' => true,
            'filters'  => [
                ['name' => 'ToInt'],
            ],
            'validators' => [
                ['name'=>'InArray', 'options'=>['haystack'=>[1, 2, 3]]]
            ],
        ]);

        // Add input for "roles" field
        $inputFilter->add([
            'class'    => ArrayInput::class,
            'name'     => 'roles',
            'required' => false,
            'filters'  => [
                ['name' => 'ToInt'],
            ],
            'validators' => [
                ['name'=>'GreaterThan', 'options'=>['min'=>0]]
            ],
        ]);

    }
}