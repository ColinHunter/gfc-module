<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 02/06/2018
 * Time: 13:15
 */

namespace User\Repository;

use Doctrine\ORM\EntityRepository;
use User\Entity\User;

// This is the custom repository class for User entity.
class UserRepository extends EntityRepository
{
    public function findOneById($id)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->where('u.id = ?1')
            ->setParameter('1', $id);

        return $queryBuilder->getQuery()->getResult();
    }
}