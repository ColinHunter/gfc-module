<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 07/11/2017
 * Time: 13:05
 */
namespace User\Service;

use User\Entity\User;
use User\Entity\Role;
use Zend\Crypt\Password\Bcrypt;
use Zend\Math\Rand;
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

/**
 * This service is responsible for adding/editing users
 * and changing user passwords.
 */
class UserManager
{
    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Role manager.
     * @var User\Service\RoleManager
     */
    private $roleManager;

    /**
     * Permission manager.
     * @var User\Service\PermissionManager
     */
    private $permissionManager;

    /**
     * PHP template renderer.
     * @var type
     */
    private $viewRenderer;

    /**
     * Application config.
     * @var type
     */
    private $config;

    /**
     * Constructs the service.
     */
    public function __construct($entityManager, $roleManager, $permissionManager, $viewRenderer, $config)
    {
        $this->entityManager = $entityManager;
        $this->roleManager = $roleManager;
        $this->permissionManager = $permissionManager;
        $this->viewRenderer = $viewRenderer;
        $this->config = $config;
    }

    /**
     * This method adds a new user.
     */
    public function addUser($data)
    {
        // Email addresses must be unique.
        if($this->checkUserExists($data['email'])) {
            throw new \Exception("User with email address " . $data['$email'] . " already exists");
        }

        // Create new User entity.
        $user = new User();
        $user->setEmail($data['email']);
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['last_name']);

        // Encrypt password and store the password in encrypted state.
        // REMOVED FOR SECURITY
        $user->setPassword($passwordHash);

        $user->setStatus($data['status']);

        $currentDate = date('Y-m-d H:i:s');
        $user->setDateCreated($currentDate);

        // Assign roles to user.
        $this->assignRoles($user, $data['roles']);

        // Add the entity to the entity manager.
        $this->entityManager->persist($user);

        // Apply changes to database.
        $this->entityManager->flush();

        //Make user directories
        $userEmailDir = str_replace('.', '', $data['email']);
        $userDir = str_replace('@', '', $userEmailDir);

        $directoryPath = getcwd() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'members'. DIRECTORY_SEPARATOR . $userDir;
        if (!is_dir($directoryPath)) {
            mkdir($directoryPath, 0755, true);
        }

        //Articles
        $articlesDir = $directoryPath . DIRECTORY_SEPARATOR . 'articles';
        if (!is_dir($articlesDir)) {
            mkdir($articlesDir, 0755, true);
        }
        $articlesVidDir = $directoryPath . DIRECTORY_SEPARATOR . 'articles' . DIRECTORY_SEPARATOR . 'video';
        if (!is_dir($articlesVidDir)) {
            mkdir($articlesVidDir, 0755, true);
        }

        //Blog
        $blogDir = $directoryPath . DIRECTORY_SEPARATOR . 'blog';
        if (!is_dir($blogDir)) {
            mkdir($blogDir, 0755, true);
        }
        $blogVidDir = $directoryPath . DIRECTORY_SEPARATOR . 'blog' . DIRECTORY_SEPARATOR . 'video';
        if (!is_dir($blogVidDir)) {
            mkdir($blogVidDir, 0755, true);
        }

        //Files
        $filesDir = $directoryPath . DIRECTORY_SEPARATOR . 'files';
        if (!is_dir($filesDir)) {
            mkdir($filesDir, 0755, true);
        }

        //Forum
        $forumDir = $directoryPath . DIRECTORY_SEPARATOR . 'forum';
        if (!is_dir($forumDir)) {
            mkdir($forumDir, 0755, true);
        }
        $forumVidDir = $directoryPath . DIRECTORY_SEPARATOR . 'forum' . DIRECTORY_SEPARATOR . 'video';
        if (!is_dir($forumVidDir)) {
            mkdir($forumVidDir, 0755, true);
        }

        //Insight
        $insightDir = $directoryPath . DIRECTORY_SEPARATOR . 'insight';
        if (!is_dir($insightDir)) {
            mkdir($insightDir, 0755, true);
        }

        //News
        $newsDir = $directoryPath . DIRECTORY_SEPARATOR . 'news';
        if (!is_dir($newsDir)) {
            mkdir($newsDir, 0755, true);
        }
        $newsVidDir = $directoryPath . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR . 'video';
        if (!is_dir($newsVidDir)) {
            mkdir($newsVidDir, 0755, true);
        }

        //Profile
        $profileDir = $directoryPath . DIRECTORY_SEPARATOR . 'profile';
        if (!is_dir($profileDir)) {
            mkdir($profileDir, 0755, true);
        }
        $profileVidDir = $directoryPath . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR . 'video';
        if (!is_dir($profileVidDir)) {
            mkdir($profileVidDir, 0755, true);
        }

        //Projects
        $projectDir = $directoryPath . DIRECTORY_SEPARATOR . 'projects';
        if (!is_dir($projectDir)) {
            mkdir($projectDir, 0755, true);
        }
        $projectVidDir = $directoryPath . DIRECTORY_SEPARATOR . 'projects' . DIRECTORY_SEPARATOR . 'video';
        if (!is_dir($projectVidDir)) {
            mkdir($directoryPath, 0755, true);
        }

        $userCreated = $this->generateNewUserToken($user);

        if($userCreated) {
            return $user;
        } else {
            return false;
        }

    }

    /**
     * Generates a new user reset token. This token is then stored in database and
     * sent to the user's E-mail address. When the user clicks the link in the E-mail message,
     * the user status is updated.
     */
    public function generateNewUserToken($user)
    {
        // Generate a token.
        $token = Rand::getString(32, '0123456789abcdefghijklmnopqrstuvwxyz', true);

        // Encrypt the token before storing it in DB.
        // REMOVED FOR SECURITY

        // Save token to DB
        $user->setPasswordResetToken($tokenHash);

        // Save token creation date to DB.
        $currentDate = date('Y-m-d H:i:s');
        $user->setPasswordResetTokenCreationDate($currentDate);

        // Apply changes to DB.
        $this->entityManager->flush();

        // Send an email to user.
        $subject = 'Validate New User Email';

        $httpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
        $userVarifyUrl = 'http://' . $httpHost . '/new-user?token=' . $token . "&id=" . $user->getId();

        // Produce HTML of password reset email
        $bodyHtml = $this->viewRenderer->render(
            'user/email/new-user-email',
            [
                'userVarifyUrl' => $userVarifyUrl,
            ]);

        $html = new MimePart($bodyHtml);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $mail = new Mail\Message();
        $mail->setEncoding('UTF-8');
        $mail->setBody($body);
        $mail->setFrom('no-reply@gfc.uk.net', 'GFC');
        $mail->addTo($user->getEmail(), $user->getFullName());
        $mail->setSubject($subject);

        // Setup SMTP transport
        $transport = new SmtpTransport();
        $options   = new SmtpOptions($this->config['smtp']);
        $transport->setOptions($options);

        $transport->send($mail);

        return true;
    }

    /**
     * This method sets new user status by new user token.
     */
    public function setNewUserByToken($id, $token)
    {
        if (!$this->validatePasswordResetToken($id, $token, true)) {
            return false;
        }

        // Find user with the given email.
        $user = $this->entityManager->getRepository(User::class)
            ->findOneById($id);

        $user->setStatus(User::STATUS_ACTIVE);

        // Remove password reset token
        $user->setPasswordResetToken(null);
        $user->setPasswordResetTokenCreationDate(null);

        $this->entityManager->flush();

        return true;
    }

    /**
     * This method updates data of an existing user.
     */
    public function updateUser($user, $data, $caller)
    {
        // Email addresses must be unique.
        if($user->getEmail()!=$data['email'] && $this->checkUserExists($data['email'])) {
            throw new \Exception("Another user with email address " . $data['email'] . " already exists");
        }

        if($caller == 'edit') {
            $user->setFirstName($data['first_name']);
            $user->setLastName($data['last_name']);
            $user->setEmail($data['email']);
            $user->setStatus($data['status']);
            // Assign roles to user.
            $this->assignRoles($user, $data['roles']);
        } elseif($caller == 'profile') {
            $user->setBiog($data['biog']);
            $user->setFirstName($data['first_name']);
            $user->setLastName($data['last_name']);
            $user->setAddress1($data['address_1']);
            $user->setAddress2($data['address_2']);
            $user->setCity($data['city']);
            $user->setCounty($data['county']);
            $user->setCountry($data['country']);
            $user->setPostCode($data['post_code']);
            $user->setShowAddress($data['show_address']);
            $user->setShowEmail($data['show_email']);
            $user->setStatus($data['status']);
            $user->setPhone($data['phone']);
            $user->setMobile($data['mobile']);
            $user->setSkype($data['skype']);
            $user->setShowContact($data['show_contact']);
            $user->setWebsite($data['website']);
            $user->setYoutube($data['youtube']);
            $user->setProfilePic($data['image_location']);
        }

        // Apply changes to database.
        $this->entityManager->flush();

        return true;
    }

    /**
     * A helper method which assigns new roles to the user.
     */
    private function assignRoles($user, $roleIds)
    {
        // Remove old user role(s).
        $user->getRoles()->clear();

        if(!is_array($roleIds)) {
            $role = $roleIds;
            $roleIds = array();
            $roleIds[] = $role;
        }

        // Assign new role(s).
        foreach ($roleIds as $roleId) {
            $role = $this->entityManager->getRepository(Role::class)
                ->find($roleId);
            if ($role==null) {
                throw new \Exception('Not found role by ID');
            }

            $user->addRole($role);
        }
    }

    /**
     * This method checks if at least one user exists in the db, if not, creates a default
     * 'Admin' user with email 'admin@example.com' and password 'Secur1ty'.
     */
    public function createAdminUserIfNotExists()
    {
        // REMOVED FOR SECURITY
    }

    /**
     * Checks whether an active user with given email address already exists in the database.
     */
    public function checkUserExists($email) {

        $user = $this->entityManager->getRepository(User::class)
            ->findOneByEmail($email);

        return $user !== null;
    }

    /**
     * Checks that the given password is correct.
     */
    public function validatePassword($user, $password)
    {
        // REMOVED FOR SECURITY

        return false;
    }

    /**
     * Generates a password reset token for the user. This token is then stored in database and
     * sent to the user's E-mail address. When the user clicks the link in the E-mail message, he/she is
     * directed to the Set Password page.
     */
    public function generatePasswordResetToken($user)
    {
        if ($user->getStatus() != User::STATUS_ACTIVE) {
            throw new \Exception('Cannot generate password reset token for inactive user ' . $user->getEmail());
        }

        // Generate a token.
        $token = Rand::getString(32, '0123456789abcdefghijklmnopqrstuvwxyz', true);

        // Encrypt the token before storing it in DB.
        // REMOVED FOR SECURITY

        // Save token to DB
        $user->setPasswordResetToken($tokenHash);

        // Save token creation date to DB.
        $currentDate = date('Y-m-d H:i:s');
        $user->setPasswordResetTokenCreationDate($currentDate);

        // Apply changes to DB.
        $this->entityManager->flush();

        // Send an email to user.
        $subject = 'Password Reset';

        $httpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
        $passwordResetUrl = 'http://' . $httpHost . '/set-password?token=' . $token . "&id=" . $user->getId();

        // Produce HTML of password reset email
        $bodyHtml = $this->viewRenderer->render(
            'user/email/reset-password-email',
            [
                'passwordResetUrl' => $passwordResetUrl,
            ]);

        $html = new MimePart($bodyHtml);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $mail = new Mail\Message();
        $mail->setEncoding('UTF-8');
        $mail->setBody($body);
        $mail->setFrom('no-reply@gfc.uk.net', 'GFC');
        $mail->addTo($user->getEmail(), $user->getFullName());
        $mail->setSubject($subject);

        // Setup SMTP transport
        $transport = new SmtpTransport();
        $options   = new SmtpOptions($this->config['smtp']);
        $transport->setOptions($options);

        $transport->send($mail);
    }

    /**
     * Checks whether the given password reset token is a valid one.
     */
    public function validatePasswordResetToken($id, $passwordResetToken, $newUser = false)
    {
        // Find user by email.
        $user = $this->entityManager->getRepository(User::class)
            ->findOneById($id);

        if($newUser) {
            if($user==null || $user->getStatus() != User::STATUS_NEW) {
                return false;
            }
        } else {
            if($user==null || $user->getStatus() != User::STATUS_ACTIVE) {
                return false;
            }
        }


        // Check that token hash matches the token hash in our DB.
        // REMOVED FOR SECURITY

        // Check that token was created not too long ago.
        $tokenCreationDate = $user->getPasswordResetTokenCreationDate();
        $tokenCreationDate = strtotime($tokenCreationDate);

        $currentDate = strtotime('now');

        if ($currentDate - $tokenCreationDate > 24*60*60) {
            return false; // expired
        }

        return true;
    }

    /**
     * This method sets new password by password reset token.
     */
    public function setNewPasswordByToken($id, $passwordResetToken, $newPassword)
    {
        if (!$this->validatePasswordResetToken($id, $passwordResetToken)) {
            return false;
        }

        // Find user with the given email.
        $user = $this->entityManager->getRepository(User::class)
            ->findOneById($id);

        if ($user==null || $user->getStatus() != User::STATUS_ACTIVE) {
            return false;
        }

        // Set new password for user
        // REMOVED FOR SECURITY
        $user->setPassword($passwordHash);

        // Remove password reset token
        $user->setPasswordResetToken(null);
        $user->setPasswordResetTokenCreationDate(null);

        $this->entityManager->flush();

        return true;
    }

    /**
     * This method is used to change the password for the given user.
     *  To change the password, you must know the old password.
     */
    public function changePassword($user, $data)
    {
        $oldPassword = $data['old_password'];

        // Check that old password is correct
        if (!$this->validatePassword($user, $oldPassword)) {
            return false;
        }

        $newPassword = $data['new_password'];

        // Check password length
        if (strlen($newPassword)<6 || strlen($newPassword)>64) {
            return false;
        }

        // Set new password for user
        // REMOVED FOR SECURITY
        $user->setPassword($passwordHash);

        // Apply changes
        $this->entityManager->flush();

        return true;
    }
}