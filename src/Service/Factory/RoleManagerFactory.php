<?php
/**
 * Created by PhpStorm.
 * User: Colin Hunter
 * Date: 02/06/2018
 * Time: 12:15
 */
namespace User\Service\Factory;

use Interop\Container\ContainerInterface;
use User\Service\RoleManager;
use User\Service\RbacManager;

/**
 * This is the factory class for RoleManager service. The purpose of the factory
 * is to instantiate the service and handle dependency injection.
 */
class RoleManagerFactory
{
    /**
     * This method creates the RoleManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $rbacManager = $container->get(RbacManager::class);

        return new RoleManager($entityManager, $rbacManager);
    }
}